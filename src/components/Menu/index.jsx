import React from "react";
import { Link } from "gatsby";

const Menu = () => (
  <nav className="navbar navbar-expand-sm navbar-light bg-light">
    <ul className="navbar-nav mr-auto">
      <li className="nav-item">
        <Link to="/" activeStyle={{ color: "#663399"}} className="nav-link" activeClassName="active">Home </Link>
      </li>
      <li className="nav-item">
        <Link to="/about" activeStyle={{ color: "#663399"}} className="nav-link" activeClassName="active"> Sobre </Link>
      </li>
      <li className="nav-item">
         <Link to="/contact" activeStyle={{ color: "#663399"}} className="nav-link" activeClassName="active"> Contato</Link>
      </li>
    </ul>
  </nav>
);

export default Menu;
