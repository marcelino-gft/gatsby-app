import React from 'react';
import GlobalStyle from '../../styles/globalStyled';
import Menu from '../Menu';


import "./layout.scss"

const Layout = ({ children }) => {
    return (
        <>
            <Menu />
            <GlobalStyle />
            <main>{children}</main>
            
        </>
    )
}


export default Layout