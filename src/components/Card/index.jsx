import React, { Component } from "react";
import { Title, Title2 } from "./styles";

class Card extends Component {
  render() {
    return (
      <div>
        <Title>
          <h3>Título do Card</h3>
          <p>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas
            corporis vero ad similique! Delectus aliquid pariatur voluptatum
            assumenda rerum cupiditate nobis illum totam. Expedita veniam odio
            id assumenda ex exercitationem?
          </p>
        </Title>
        <Title2>
            <h4>Subtítulo</h4>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Molestias reiciendis exercitationem corrupti nemo inventore quo vel rerum est, quaerat magni totam cupiditate rem incidunt voluptatem assumenda modi ab vero qui?</p>
        </Title2>
      </div>
    );
  }
}

export default Card;
