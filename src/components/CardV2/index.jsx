import React from "react";
import * as S from './styled'

const CardV2 = (props) => {
  return (
    <S.CardV2Wrapper className="card" background="#e3a617">
      <div class="card-body">
        <h5 class="card-title">{props.titleCard}</h5>
        <p class="card-text">{props.textCard}</p>
        <a href="#" class={`btn btn-${props.colorButton}`}>Visitar</a>
      </div>
    </S.CardV2Wrapper>
  );
};

export default CardV2;

