import * as React from "react"
import Layout from "../components/Layout";

// styles


// markup
const IndexPage = () => {
  return (
    <Layout>
      <title>Home</title>
      <div className="container">
        <div className="row">
          <div className="col-12 py-4">
            <h1>Página: Principal</h1>
            <p>Bem-vindo ao site</p>
            <a href="https://www.gatsbyjs.com/docs/" rel="noreference" target="_blank">Gatsby Documentation</a>
          </div>
        </div>
      </div>
    </Layout>
  )
}

export default IndexPage
