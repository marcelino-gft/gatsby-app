import React from "react";
import Layout from "../components/Layout"
import Card from "../components/Card";

const Contact = () => {
  const text = "Subtítulo";

  return (
    <Layout>
      <h1>Página: Contato</h1>
      <h2>{text}</h2>
      <Card />
    </Layout>
  );
};

export default Contact;
