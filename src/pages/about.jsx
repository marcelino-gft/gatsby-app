import React from "react";
import Layout from "../components/Layout"
import CardV2 from "../components/CardV2";
 
const About = () => {
  const text = "Subtítulo";

  return (
    <Layout>
        <h1>Página: Sobre</h1>
        <h2>{text}</h2>
        <div className="container">
          <div className="row">
            <div className="col-4">
              <CardV2 
                titleCard="Título 01"
                textCard="Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae recusandae quam non delectus! Tempora cum placeat, quos iusto quam, dolorem ipsum repellendus voluptatum in vitae a natus neque magnam maiores!"
                colorButton="primary"
              />
            </div>
            <div className="col-4">
            <CardV2 
                titleCard="Título 02"
                textCard="Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae recusandae quam non delectus! Tempora cum placeat, quos iusto quam, dolorem ipsum repellendus voluptatum in vitae a natus neque magnam maiores!"
                colorButton="warning"
              />
            </div>
            <div className="col-4">
            <CardV2 
                titleCard="Título 03"
                textCard="Lorem ipsum dolor sit amet consectetur adipisicing elit. Beatae recusandae quam non delectus! Tempora cum placeat, quos iusto quam, dolorem ipsum repellendus voluptatum in vitae a natus neque magnam maiores!"
                colorButton="danger"
              />
            </div>
          </div>
        </div>
    </Layout>
  );
};

export default About;
